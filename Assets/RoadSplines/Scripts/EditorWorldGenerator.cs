﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WorldGenerate))]
public class EditorWorldGenerator : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WorldGenerate world = (WorldGenerate)target;
        CurveImplementation curve = world.GetComponentInChildren<CurveImplementation>();

        if (GUILayout.Button("Generate World Road"))
        {
            world.DrawWorld(true);
            curve.DrawSpline(true);
            curve.GenerateMesh();
        }
    }
}


