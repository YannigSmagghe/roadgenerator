﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WorldGenerate : MonoBehaviour
{
    public GameObject plane;
    public CurveImplementation curve;
    public List<GameObject> listOfPoint;
    public Vector3 center;

    public int nbPoint;

    public Vector3 size;
    // Start is called before the first frame update
    void Start()
    {
        center = plane.transform.position;
        size = plane.transform.localScale;
    }

    void Update()
    {
        DrawWorld(false);
    }

    public void DrawWorld(bool store)
    {
       
        if (store)
        {
            foreach (var sphere in listOfPoint)
            {
                DestroyImmediate(sphere);
            }
            listOfPoint.Clear();

            for (int i = 0; i < nbPoint; i++)
            {
                Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = pos;
                listOfPoint.Add(sphere);

            }
        }

        curve.ControlPoints = listOfPoint;
       


    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center, size);

    }


}
